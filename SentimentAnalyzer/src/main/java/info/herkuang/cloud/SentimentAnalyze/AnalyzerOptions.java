package info.herkuang.cloud.SentimentAnalyze;

import org.kohsuke.args4j.Argument;

/**
 * Team12
 * Heguang Miao / 718712
 * Nianbo Wang / 705138
 * Rui Yang / 741498
 * Jing Gao / 721770
 * Ye Gu / 685883
 */
public class AnalyzerOptions {
    @Argument(required = true, usage = "rank of the process")
    public int rank;
}
