import twitter4j.*;

/**
 * Team12
 * Heguang Miao / 718712
 * Nianbo Wang / 705138
 * Rui Yang / 741498
 * Jing Gao / 721770
 * Ye Gu / 685883
 */
public class SearchCrawler {
    public static void main(String[] args) throws TwitterException, InterruptedException {
        Twitter twitter = TwitterFactory.getSingleton();
        Query query = new Query("@VictoriaPolice");
        query.setCount(10);
        QueryResult res = twitter.search(query);
        for(Status status : res.getTweets()) {
            System.out.println(status);
        }
        Thread.sleep(1000);
        while (res.hasNext()) {
            query = res.nextQuery();
            res = twitter.search(query);
            for(Status status : res.getTweets()) {
                System.out.println(status);
            }
            Thread.sleep(1000);
        }
    }
}
