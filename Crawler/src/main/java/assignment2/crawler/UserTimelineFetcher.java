package assignment2.crawler;

import info.herkuang.cloud.commons.PutIntoCouchDb;
import info.herkuang.cloud.commons.RabbitMQCoordinator;
import org.lightcouch.DocumentConflictException;
import twitter4j.*;

/**
 * Team12
 * Heguang Miao / 718712
 * Nianbo Wang / 705138
 * Rui Yang / 741498
 * Jing Gao / 721770
 * Ye Gu / 685883
 */
public class UserTimelineFetcher implements Runnable {

    private long userId = -1;

    public UserTimelineFetcher(long userId) {
        this.userId = userId;
    }

    @Override
    public void run() {
        try {
            // we need to sleep for a while in case of being blocked by twitter
            fetchTimeline();
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    private void fetchTimeline() throws TwitterException {
        if(this.userId == -1) {
            return;
        }

        Twitter twitter = TwitterFactory.getSingleton();
        Paging p = new Paging();
        p.setCount(200);
        ResponseList<Status> res = twitter.getUserTimeline(userId, p);
        for(Status status : res) {
            String twitterJsonString = TwitterObjectFactory.getRawJSON(status);
            try {
                String saved = PutIntoCouchDb.sharedInstance.saveOnCouchDb(twitterJsonString);
                RabbitMQCoordinator.singleton.sendMessage(saved);
            } catch (DocumentConflictException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    public static void main(String[] args) {
        UserTimelineFetcher fetcher = new UserTimelineFetcher(453545817);
        fetcher.run();
    }
}
