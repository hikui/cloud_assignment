package info.herkuang.cloud.SentimentAnalyze;

import com.google.gson.JsonObject;
import org.lightcouch.CouchDbClient;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * Team12
 * Heguang Miao / 718712
 * Nianbo Wang / 705138
 * Rui Yang / 741498
 * Jing Gao / 721770
 * Ye Gu / 685883
 */
public class CouchDBCoordinator {
    private CouchDbClient dbClient = null;
    private String viewName = null;
    private TweetPreprocessor preprocessor = new TweetPreprocessor();
    private SentimentAnalyzer analyzer = new SentimentAnalyzer();

    public CouchDBCoordinator(String viewName) throws IOException {
        FileInputStream input = new FileInputStream("couchdb.properties");
        // load a properties file
        Properties prop = new Properties();
        prop.load(input);
        String name = prop.getProperty("couchdb.name");
        String host = prop.getProperty("couchdb.host");
        int port = Integer.parseInt(prop.getProperty("couchdb.port"));
        this.dbClient = new CouchDbClient(name, true, "http", host, port, null, null);
        this.viewName = viewName;
    }

    public void updateDocument(JsonObject document) {
        dbClient.update(document);
    }

    public void processTweets(int startRow, int endRow) throws Exception {
        if(this.viewName == null) {
            throw new Exception("View must be specified!");
        }
        // we process tweets page by page, in order to save resources
        int chunkSize = Math.min(5000, endRow - startRow);
        int offset = startRow;
        while(offset < endRow) {
            List<JsonObject> list = dbClient.view(this.viewName)
                    .includeDocs(true)
                    .skip(offset)
                    .limit(chunkSize)
                    .query(JsonObject.class);
            for(JsonObject json : list) {
                String text = json.get("text").getAsString();
                String preprocessedTweet = preprocessor.process(text);
                int sentiment = analyzer.findSentiment(preprocessedTweet);
                json.addProperty("info.herkuang.sentiment", sentiment);
                dbClient.update(json);
            }
            offset += chunkSize;
        }
    }

    public static void main(String[] args) throws Exception {
        CouchDBCoordinator coordinator = new CouchDBCoordinator("sentiment/not_marked");
        coordinator.processTweets(0, 10);
    }

}
