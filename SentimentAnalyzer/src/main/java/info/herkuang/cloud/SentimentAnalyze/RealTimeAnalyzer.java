package info.herkuang.cloud.SentimentAnalyze;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.rabbitmq.client.*;
import info.herkuang.cloud.commons.PutIntoCouchDb;
import info.herkuang.cloud.commons.RabbitMQCoordinator;

import java.io.IOException;

/**
 * Team12
 * Heguang Miao / 718712
 * Nianbo Wang / 705138
 * Rui Yang / 741498
 * Jing Gao / 721770
 * Ye Gu / 685883
 */
public class RealTimeAnalyzer {
    public static void main(String[] args) throws IOException {
        final Channel channel = RabbitMQCoordinator.singleton.channel;
        channel.basicQos(1);
        final Consumer consumer = new DefaultConsumer(channel) {
            private JsonParser parser = new JsonParser();
            private TweetPreprocessor preprocessor = new TweetPreprocessor();
            private SentimentAnalyzer analyzer = new SentimentAnalyzer();
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                channel.basicAck(envelope.getDeliveryTag(), false);
                JsonObject json = parser.parse(message).getAsJsonObject();
                if(json == null) {
                    return;
                }
                String text = json.get("text").getAsString();
                String lang = json.get("lang").getAsString();
                if (!"en".equals(lang)) {
                    // sentiment analysis only supports English
                    return;
                }
                String processed = preprocessor.process(text);
                if (processed.equals("")) {
                    return;
                }
                int sentiment = analyzer.findSentiment(processed);
                json.addProperty("info.herkuang.sentiment", sentiment);
                PutIntoCouchDb.sharedInstance.updateDocument(json);
            }
        };
        try {
            System.out.println("Start consume");
            RabbitMQCoordinator.singleton.basicConsume(consumer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
