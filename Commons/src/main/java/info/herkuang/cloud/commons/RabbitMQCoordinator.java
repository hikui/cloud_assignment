package info.herkuang.cloud.commons;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

/**
 * Team12
 * Heguang Miao / 718712
 * Nianbo Wang / 705138
 * Rui Yang / 741498
 * Jing Gao / 721770
 * Ye Gu / 685883
 */
public class RabbitMQCoordinator {

    public static final RabbitMQCoordinator singleton = new RabbitMQCoordinator();

    private String MQ_QUEUE = "Twitter";
    private static final Logger log = LogManager.getLogger(RabbitMQCoordinator.class);

    public Channel channel = null;
    private Connection connection = null;


    private RabbitMQCoordinator() {
        // setup rabbitmq
        try {
            Properties mqProperties = LocalPropertiesLoader.loadProperties("rabbitmq.properties");
            String host = mqProperties.getProperty("rabbitmq.host");
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(host);
            connection = factory.newConnection();
            channel = connection.createChannel();
            String queueName = mqProperties.getProperty("rabbitmq.queuename","Twitter");
            MQ_QUEUE = queueName;
            channel.queueDeclare(MQ_QUEUE, false, false, false, null);

        } catch (TimeoutException | IOException e) {
            log.error(e.getMessage());
        }
    }

    public void sendMessage(String msg) {
        if(this.channel == null) {
            return;
        }
        try {
            channel.basicPublish("", MQ_QUEUE, null, msg.getBytes());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public void basicConsume(Consumer consumer) throws IOException {
        channel.basicConsume(MQ_QUEUE, false, consumer);
    }
}
