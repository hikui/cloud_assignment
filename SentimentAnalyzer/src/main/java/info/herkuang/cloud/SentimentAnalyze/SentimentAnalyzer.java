package info.herkuang.cloud.SentimentAnalyze;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

import java.util.Properties;

/**
 * Created by HeguangMiao on 24/04/2016.
 */
public class SentimentAnalyzer {
    public int findSentiment(String line) {
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
        float averageSentiScore = 0.0f;
        if (line != null && line.length() > 0) {
            Annotation annotation = pipeline.process(line);
            for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
                Tree tree = sentence.get(SentimentCoreAnnotations.AnnotatedTree.class);
                int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
                String partText = sentence.toString();
                averageSentiScore += ((float)sentiment) * partText.length();
            }
            averageSentiScore /= line.length();
            if (averageSentiScore > 2) {
                return 1;
            } else if (averageSentiScore == 2) {
                return 0;
            } else if (averageSentiScore < 2) {
                return -1;
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        SentimentAnalyzer analyzer = new SentimentAnalyzer();
        System.out.println(analyzer.findSentiment("I'm at Glen Waverley Station (Glen Waverley Station, Glen Waverley)"));
    }
}
