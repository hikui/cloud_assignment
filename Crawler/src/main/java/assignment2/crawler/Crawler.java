package assignment2.crawler;

import java.io.*;
import java.util.Properties;
import java.util.concurrent.*;

import info.herkuang.cloud.commons.LocalPropertiesLoader;
import info.herkuang.cloud.commons.PutIntoCouchDb;
import info.herkuang.cloud.commons.RabbitMQCoordinator;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import twitter4j.*;

/**
 * Team12
 * Heguang Miao / 718712
 * Nianbo Wang / 705138
 * Rui Yang / 741498
 * Jing Gao / 721770
 * Ye Gu / 685883
 */

class RejectedExecutionHandlerImpl implements RejectedExecutionHandler {

    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
        System.out.println(r.toString() + " is rejected");
    }

}

/**
 * Receives tweets from Twitter streaming API. You need to pass the right
 * parameters in the config.properties file.
 *
 */
public class Crawler {
    private static final Logger log = LogManager.getLogger(Crawler.class);

    public static void main(String[] args) throws TwitterException, IOException {

        RejectedExecutionHandlerImpl rejectionHandler = new RejectedExecutionHandlerImpl();
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        final ThreadPoolExecutor executorPool = new ThreadPoolExecutor(5, 8, 10, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(5), threadFactory, rejectionHandler);

        RawStreamListener rawListener = new RawStreamListener() {

            private JsonParser parser = new JsonParser();


            @Override
            public void onMessage(String rawString) {
                PutIntoCouchDb putIntoCouchDb = PutIntoCouchDb.sharedInstance;
                String saved = putIntoCouchDb.saveOnCouchDb(rawString);
                // send to rabbitmq if possible
                RabbitMQCoordinator.singleton.sendMessage(saved);
                JsonObject jobj = parser.parse(rawString).getAsJsonObject();
                JsonObject userJson = jobj.get("user").getAsJsonObject();
                // fetch user timeline
                long userID = userJson.get("id").getAsLong();
                executorPool.execute(new UserTimelineFetcher(userID));
            }

            @Override
            public void onException(Exception ex) {

            }
        };


        // Relies on twitter4j.properties
        TwitterStream twitterStream = new TwitterStreamFactory().getInstance();

        twitterStream.addListener(rawListener);
        FilterQuery filterQuery = new FilterQuery();

        double SWLong = 0;
        double SWLat = 0;
        double NELong = 0;
        double NELat = 0;

        try {

            Properties prop = LocalPropertiesLoader.loadProperties("geolocation.properties");
            SWLong = Double.parseDouble(prop.getProperty("info.herkuang.sw.long"));
            SWLat = Double.parseDouble(prop.getProperty("info.herkuang.sw.lat"));
            NELong = Double.parseDouble(prop.getProperty("info.herkuang.ne.long"));
            NELat = Double.parseDouble(prop.getProperty("info.hekruang.ne.lat"));
        } catch (Exception e) {
            // it's okay to have problem. we'll use the default value
            log.error("Read location properties failed, use default ones");
            SWLong = 144.216185;
            SWLat = -38.279906;
            NELong = 145.836275;
            NELat = -37.430437;
        }

        double[][] locations = { {SWLong, SWLat }, {NELong, NELat} };
        filterQuery.locations(locations);
        twitterStream.filter(filterQuery);
    }

}
