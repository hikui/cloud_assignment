package info.herkuang.cloud.commons;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Team12
 * Heguang Miao / 718712
 * Nianbo Wang / 705138
 * Rui Yang / 741498
 * Jing Gao / 721770
 * Ye Gu / 685883
 */
public class LocalPropertiesLoader {
    public static Properties loadProperties(String path) throws IOException {
        InputStream input = null;
        try {
            input = new FileInputStream(path);
            Properties prop = new Properties();
            prop.load(input);
            return prop;
        } catch (IOException e) {
            throw e;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
