package info.herkuang.cloud.SentimentAnalyze;

import com.google.common.base.Joiner;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Team12
 * Heguang Miao / 718712
 * Nianbo Wang / 705138
 * Rui Yang / 741498
 * Jing Gao / 721770
 * Ye Gu / 685883
 */
public class TweetPreprocessor {
    public String process(String rawTweetText) {
        String processedTweet = null;
        // 0. remove RT if exist
        processedTweet = rawTweetText.replaceFirst("RT ", "");
        // 1. remove hashtags
        Pattern pt = Pattern.compile("#\\w+");
        Matcher matcher = pt.matcher(rawTweetText);
        ArrayList<String> hashtags = new ArrayList<String>();
        while(matcher.find()) {
            hashtags.add(matcher.group().substring(1)); // remove #
        }
        String hashtagSentence = Joiner.on(" ").join(hashtags);
        processedTweet = processedTweet.replaceAll("#\\w+", "");
        // 2. remove @
        processedTweet = processedTweet.replaceAll("@", "");
        // 3. remove links
        processedTweet = processedTweet.replaceAll("https?://\\S*","");
        processedTweet += " " + hashtagSentence;
        // 4. trim
        processedTweet = processedTweet.trim();
        return processedTweet;
    }

    public static void main(String[] args) {
        TweetPreprocessor processor = new TweetPreprocessor();
        System.out.println(processor.process("I'm at Glen Waverley Station (Glen Waverley Station, Glen Waverley) http://t.co/AX9Td5M"));
    }
}
