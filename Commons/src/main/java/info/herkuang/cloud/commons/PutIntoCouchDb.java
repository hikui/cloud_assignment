package info.herkuang.cloud.commons;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.lightcouch.CouchDbClient;
import org.lightcouch.DocumentConflictException;
import org.lightcouch.Response;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Team12
 * Heguang Miao / 718712
 * Nianbo Wang / 705138
 * Rui Yang / 741498
 * Jing Gao / 721770
 * Ye Gu / 685883
 */
public class PutIntoCouchDb {

    public static PutIntoCouchDb sharedInstance = new PutIntoCouchDb();

    // config relies on couchdb.properties
    private CouchDbClient dbClient = null;
    private Properties prop = new Properties();

    private PutIntoCouchDb() {
        try {
            prop = LocalPropertiesLoader.loadProperties("couchdb.properties");
            String name = prop.getProperty("couchdb.name");
            String host = prop.getProperty("couchdb.host");
            int port = Integer.parseInt(prop.getProperty("couchdb.port"));
            dbClient = new CouchDbClient(name, true, "http", host, port, null, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Save on CouchDB
     * @param twitterJson
     * @return Saved JSON with _id and _rev
     * @throws DocumentConflictException
     */
    public String saveOnCouchDb(String twitterJson)throws DocumentConflictException {
        Gson gson = new Gson();
        JsonObject jsonObj = gson.fromJson(twitterJson, JsonObject.class);
        String id = jsonObj.get("id").getAsString();
        jsonObj.addProperty("_id", id);
        Response resp = dbClient.save(jsonObj);
        jsonObj.addProperty("_rev", resp.getRev());
        return jsonObj.toString();
    }

    public void updateDocument(JsonObject document) {
        dbClient.update(document);
    }
}
